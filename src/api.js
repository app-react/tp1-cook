import axios from "axios"

export const  callApi = axios.create({
  baseURL: `https://www.themealdb.com/api/json/v1/1/`
})