import React from 'react';

const Layout = ({children}) => {
  return (
    <div className="bg-slate-100 min-h-screen pb-20">
      <header className="min-h-20 py-8 flex flex-col justify-center items-center">
        <h1 className="text-3xl font-semibold rounded border-2 border-red-500 px-4 py-2 text-red-500">React Cook</h1>
        <p className="text-xs font-semibold mt-2">
          with api : 
          <a href="https://www.themealdb.com/api" className="text-red-500 hover:underline ml-3">
            themealdb
          </a>
        </p>
      </header>
      {children}
    </div>
  );
};

export default Layout;