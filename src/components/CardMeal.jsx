import React from 'react';

const CardMeal = ({meal}) => {
  return (
    <article className="rounded bg-white shadow-md px-2 py-3">
      <header className="flex flex-col justify-center items-center mb-3">
        <h2 className="text-lg font-semibold mb-3">{meal.strMeal}</h2>
        <img src={meal.strMealThumb} alt="" className="h-auto w-3/4"/>
        <p>origin : {meal.strArea}</p>
      </header>
      <div className="px-4">
        <p className="max-h-28 overflow-y-scroll scrollbar-hide">
          {meal.strInstructions}
        </p>

      </div>
    </article>
  );
};

export default CardMeal;