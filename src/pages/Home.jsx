import React, { useEffect, useRef, useState } from 'react';
import { callApi } from '../api';
import CardMeal from '../components/CardMeal';
import Layout from '../components/Layout';
import debounce from "lodash/debounce"

const Home = () => {

  const [inputSearch, setInputSearch] = useState('tomato')
  const [meals, setMeals] = useState([])

  useEffect( () => {
    callApi
      .get('/search.php', {
        params: {
          s: inputSearch
        }
      })
      .then(({data})=> data.meals ? setMeals(data.meals) : setMeals([]))
  }, [inputSearch])

  const debouncedSearch = useRef(
    debounce((criteria) => {
      // console.log("🚀 ~ file: Home.jsx ~ line 24 ~ debounce ~ criteria", criteria)
      setInputSearch(criteria.target.value);
    }, 300)
  ).current;

  return (
    <Layout>

      <div className="bg-white mb-5 mx-auto max-w-md">
        <input 
          type="text" 
          placeholder="Recherche ..."
          onChange={debouncedSearch}
        />
      </div>

      <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 gap-5 px-5">
        {meals.map(meal => (
          <CardMeal key={meal.idMeal} meal={meal}/>
        ))}

      </section>

    </Layout>
  );
};

export default Home;